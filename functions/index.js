'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

const spawn = require('child-process-promise').spawn;
const path = require('path');
const os = require('os');
const fs = require('fs');
const crc = require('crc');

const chat = require('./Util/Chat')(admin);
const image = require('./Util/Image')(admin, spawn, crc, path);

exports.uploadImageSuccess = functions.storage.object().onFinalize(async object => {
	let tempFilePath;
	try {
		// Exit if this is triggered on a file that is not an image.
		if (!object.contentType.startsWith('image/')) {
			return console.log('This is not an image.');
		}

		const filePath = object.name;
		const fileName = path.basename(filePath);
		// Exit if the image is already a thumbnail.
		if (fileName.startsWith('thumb_')) {
			return console.log('Already a Thumbnail.');
		}

		// Exit if the image is already renamed.
		if (fileName.startsWith('tw_')) {
			return console.log('image is already renamed.');
		}

		// console.log(object);
		// Download image file to temporary
		tempFilePath = path.join(os.tmpdir(), fileName);
		await admin
			.storage()
			.bucket(object.bucket)
			.file(object.name)
			.download({ destination: tempFilePath });

		const newObject = await image.renameImage(object, tempFilePath);
		await image.deleteImage(object);
		const thumbnailObject = await image.createThumbnail(newObject, tempFilePath);
		await chat.createChatThread(newObject, thumbnailObject, object.generation);
		await chat.updateChatList(newObject);

		console.log(`upload success ${fileName}`);
	} catch (error) {
		return console.error(error);
	}

	// Once the thumbnail has been uploaded delete the local file to free up disk space.
	return fs.unlinkSync(tempFilePath);
});

exports.sendChatNotification = functions.database.ref('/chatThreads/{room}/{chatRandomRef}').onCreate(async (snapshot, context) => {
	try {
		const room = context.params.room;
		const newChat = snapshot.val();
		console.log('new chat', newChat);
		const chatRefKeys = room.split('_');
		console.log('members', chatRefKeys);

		let chatLists = (await admin
			.database()
			.ref(`/chatLists/${chatRefKeys[0]}/${room}`) // member[0] because want to get member name only
			.once('value')).val();

		console.log('chatLists', chatLists);
		const senderChatRefKey = newChat.sender.split('_')[0];
		console.log('sender', senderChatRefKey);
		const notiChatRefKeys = chatRefKeys.filter(chatRefKey => chatRefKey !== senderChatRefKey);
		console.log('noti members', notiChatRefKeys);
		const notis = [];
		for (let i = 0; i < notiChatRefKeys.length; i++) {
			const users = (await admin
				.database()
				.ref(`/users/${notiChatRefKeys[i]}`)
				.once('value')).val();

			const type = newChat.type !== 'image' ? 'message' : 'image';
			const payload = {
				notification: {
					title: `You have a new ${type} from ${chatLists.members[senderChatRefKey]}`,
					body: newChat.message,
					// icon: follower.photoURL
				},
			};

			const notiChatRefKey = notiChatRefKeys[i].split('&')[0];
			if (notiChatRefKey === 'twister' || notiChatRefKey === 'seller') {
				Object.keys(users).forEach(chatRefKey => {
					const token = users[chatRefKey];
					notis.push(admin.messaging().sendToDevice(token, payload));
				});
			} else {
				notis.push(admin.messaging().sendToDevice(users.remoteNotificationToken, payload));
			}
		}
		console.log('noti', notis);
		return Promise.all(notis);
	} catch (error) {
		return console.error(error);
	}
});
