const imageExpireInMilliSecond = 1000 * 60 * 60 * 24 * 365 // 1 year

module.exports = {
    imageExpireInMilliSecond
};