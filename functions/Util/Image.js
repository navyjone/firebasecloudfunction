'use strict';
const config = require('../config');

const image = (admin, spawn, crc, path) => {
	const createThumbnail = async (newObject, tempFilePath) => {
		try {
			await spawn('convert', [tempFilePath, '-thumbnail', '480x320>', tempFilePath]);
			// console.log('Thumbnail created at', tempFilePath);
			// We add a 'thumb_' prefix to thumbnails file name. That's where we'll upload the thumbnail.
			const thumbFileName = `thumb_${newObject.fileName}`;
			const thumbFilePath = path.join(path.dirname(newObject.filePath), thumbFileName);
			// console.log('thumb file name ', thumbFileName);
			// console.log('thumb file path ', thumbFilePath);

			const newThumbnailObject = await admin
				.storage()
				.bucket(newObject.bucket)
				.upload(tempFilePath, {
					destination: thumbFilePath,
					metadata: newObject.metadata,
				});

			const signedUrl = await getSignedUrl(newThumbnailObject);
			console.log('create thumbnail success.');

			return {
				bucket: newObject.bucket,
				fileName: thumbFileName,
				filePath: thumbFilePath,
				signedUrl: signedUrl,
			};
		} catch (error) {
			console.error('create thumbnail fail', error);
			throw error;
		}
	};

	const renameImage = async (object, tempFilePath) => {
		try {
			const filePath = object.name; // File path in the bucket.
			const contentType = object.contentType; // File content type.
			//const metageneration = object.metageneration; // Number of times metadata has been generated. New objects have a value of 1.
			const fileName = path.basename(filePath);
			const fileNameSplit = fileName.split('.');
			const fileType = fileNameSplit[fileNameSplit.length - 1];

			// Upload new file name
			const newFileName = `tw_${crc.crc32(fileName).toString(16)}_${object.generation}.${fileType}`;
			const newFilePath = path.join(path.dirname(filePath), newFileName);
			const newObject = await admin
				.storage()
				.bucket(object.bucket)
				.upload(tempFilePath, {
					destination: newFilePath,
					metadata: {
						contentType: contentType,
					},
				});

			const signedUrl = await getSignedUrl(newObject);
			console.log('rename image success.');
			// console.log('new object', newObject);
			// console.log('new object []', newObject[0]);
			// console.log('new object [{}]', newObject[0].metadata);

			return {
				bucket: object.bucket,
				metadata: {
					contentType: contentType,
				},
				fileName: newFileName,
				filePath: newFilePath,
				signedUrl: signedUrl,
			};
		} catch (error) {
			console.error('rename image fail', error);
			throw error;
			// console.error(new Error('rename image fail', error));
		}
	};

	const deleteImage = async object => {
		try {
			await admin
				.storage()
				.bucket(object.bucket)
				.file(object.name)
				.delete();
		} catch (error) {
			console.error('delete image fail', error);
			throw error;
		}
	};

	const getSignedUrl = async newObject => {
		try {
			const newMeta = newObject[0].metadata;
			// console.log('new meta', newMeta);
			const option = {
				action: 'read',
				expires: Date.now() + config.imageExpireInMilliSecond,
			};

			const signedUrls = await admin
				.storage()
				.bucket(newMeta.bucket)
				.file(newMeta.name)
				.getSignedUrl(option);

			return signedUrls[0];
		} catch (ex) {
			console.error('get signed url error', ex);
			return '';
		}
	};

	return {
		createThumbnail,
		renameImage,
		deleteImage,
	};
};

module.exports = image;
