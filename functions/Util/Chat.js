'use strict';

const chat = admin => {
	const createChatThread = async (newObject, thumbnailObject, imageRef) => {
		try {
			const room = newObject.filePath.split('/')[1];
			// console.log('create chat thread room name ', room);
			const uniqueId = newObject.filePath.split('/')[2];
			// console.log('create chat thread uniqueId ', uniqueId);
			await admin
				.database()
				.ref(`chatThreads/${room}`)
				.push({
					message: '',
					sender: uniqueId,
					readDate: {},
					datetime: Date.now(),
					image: newObject.signedUrl,
					thumbnail: thumbnailObject.signedUrl,
					type: 'image',
					imageRef: imageRef,
				});

			console.log('create chat thread success.');
		} catch (error) {
			console.error('create chat thread fail', error);
			throw error;
		}
	};

	const updateChatList = async newObject => {
		try {
			const room = newObject.filePath.split('/')[1];
			// console.log('create chat list room name ', room);
			const senderUniqueId = newObject.filePath.split('/')[2];
			// console.log('create chat list uniqueId ', senderUniqueId);
			const members = room.split('_');
			for (let i = 0; i < members.length; i++) {
				// console.log('chat list: ', `chatLists/${member}/${room}`);
				const member = members[i];
				await admin
					.database()
					.ref(`chatLists/${member}/${room}`)
					.transaction(currentRoom => {
						let unreadAdd = 1;
						const chatRefKey = senderUniqueId.split('_')[0];
						const message = currentRoom && currentRoom.members ? `${currentRoom.members[chatRefKey]} sent a photo` : 'You got a new photo';
						if (!senderUniqueId || senderUniqueId === member) {
							unreadAdd = 0;
						}

						if (currentRoom && currentRoom.unread) {
							unreadAdd = currentRoom.unread + unreadAdd;
						}

						return {
							...currentRoom,
							message: message,
							unread: unreadAdd,
							datetime: Date.now(),
						};
					});
			}

			console.log('create chat list success.');
		} catch (error) {
			console.error('create chat list fail', error);
			throw error;
		}
	};

	return {
		createChatThread,
		updateChatList,
	};
};

module.exports = chat;
